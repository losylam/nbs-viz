const path = require('path');


module.exports = mode => {
    let base = {
        entry: './src/index.js',
        mode: 'production',
        devServer: {
            contentBase: './dist',
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        'css-loader',
                    ],
                },
                {
                    test: /\.(png|svg|jpg|gif|otf)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        outputPath: (file) => {
                            let path = file.split("src/")[1];
                            return path;
                        }
                    }
                },
            ],
        },
    };

    if (mode == 'development'){
        base.mode = 'development';
        base.devtool = 'inline-source-map';
    }

    return base;
};

