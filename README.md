Nbs-viz
===

Nbs-viz, The Nature Based Solutions explorer, is an interactive data visualization application that allow user to explore Nature Based Solutions (NBS) and their relations with urban challenges (UC) as developped during the Nature4Cities project.

Is ia a single page web application build with the d3js javascript library.

## File structure

```
.
├── data           -> backend script (xls to json)
├── dist           -> frontend
│   ├── assets     -> icons and font
│   ├── data       -> NBS data and translation in json files
│   ├── images     -> NBS pictures
│   ├── pdf        -> NBS frontpage
│   ├── bundle.js  -> js + css
│   └── index.html -> front page
├── src
│   ├── assets
│   ├── css
│   ├── index.js
│   ├── nbs_viz_force.js
│   └── style.css
├── svg
├── package.json
├── package-lock.json
├── README.md
└── webpack.config.js
```

## Back office

### Data management

A google document spreadsheet contains all the data that are used by the visualization, including translation. It is composed by the following tables:
* **Matrix** each row describe a NBS as follow:
  * a single name and identifier (ID) 
  * the typology classification (category, sub-category and class)
  * the scale (entity, neightborhoo or city)
  * the relation with UC that can be a benefit (cell value = 2) or a co-benefit (cell value = 1) 
* **traduction table** contains all the translations for the typology, the challenges and sub-challenges name and all the text that is used in the user interface (legend, manual, section title...)
* **infos NBS** 
  * name and short description of each NBS and translation by columns
  * the name of the pictures related to the NBS
* **Challenges** contains the description of challenges and subchallengs identified by the challenge/sub-challenge ID, and translation by columns
* **pictures** contains caption of each picture identified by picture ID, and translation by columns

This spreadsheet file is supposed to be processed by a javascript script that return one json file by language. These json files are loaded by the nbs-viz page to be displayed on the visualization.

### Other ressources

#### Pictures

Pictures are stored in the `dist/images` directory in two sub-directory:
- `thumb` contains thumbnail pictures (max dimension = 200px)
- `fullsize` contains "fullsize" pictures (max dimension = 1200px)

Each picture is a jpeg file named with the picture ID (case sensitive) and the extension `.jpg`

#### Factsheet

Factsheets are stored in the `dist/pdf` directory that contains one sub-directory by language. The file names for each factsheet is templated `NBSID_LANG.pdf` with `NBSID` the nbs ID (for example "CP_diversification") and `LANG` the language abbreviation (for example "en" for english)


## Build

### Dependencies

```
npm install
```

### Production mode


```
npm run build
```

### Development

```
npm start
```
