import * as d3 from "d3";

import scale_city from './assets/nbs_City.png';
import scale_district from './assets/nbs_District.png';
import scale_Entity from './assets/nbs_Entity.png';


let new_nbs_url = "http://enquetes.plante-et-cite.fr/limesurvey/index.php/129735?newtest=Y&lang=en";

let sim;
let cur_elm;

let main_node_size = 50;

let create_new_button = true;

const color_scale = ["rgb(0, 164, 163)",
    "rgb(0, 113, 103)",
    "rgb(57, 170, 62)",
    "rgb(224, 236, 60)",
    "rgb(255, 155, 11)"
];

const color = function(d) {
    let c = color_scale[d.depth];
    if (d.hi == true){
        return c;
    }else{
        return d3.color(c).darker(4);
    }
};

function url_exists(url, callback){
    let http = new XMLHttpRequest();
    http.open("HEAD", url, true);
    // http.timeout = 200;
    http.onload = function(e){
        console.log("[url_exists] status", http.status);
        if (http.status == 404){
            callback();
        }
    };
    http.onerror = function(e){
        console.log(e);
        callback();
    };
    http.send();
    return http.status!=404;
};

function node_size(d){
    if (d.data.name == "empty") {
        return 0;
    } else {
        return main_node_size / (d.depth + 1);
    }
};

let cur_url = new URL(document.URL);
var nav_lang = cur_url.searchParams.get("hl");

var nbs_url = cur_url.searchParams.get("nbs");

if (nav_lang == null){
    nav_lang = navigator.language;
}

let lst_lang = ["fr", "de", "hu", "it", "es", "tr"];
let lang = '';


if (lst_lang.indexOf(nav_lang) == -1){
    nav_lang = 'en';
}

if (nav_lang == "fr"){
    lang = "Français";
}else if (nav_lang == "hu"){
    lang = "Magyar";
}else if (nav_lang == "de"){
    lang = "Deutsch";
}else if (nav_lang == "it"){
    lang = "Italian";
}else if (nav_lang == "es"){
    lang = "español";
}else if (nav_lang == "tr"){
    lang = "Turkish";
}else{
    lang = "English";
};

console.log("data url", "data/nbs-"+ nav_lang + ".json");

d3.json("data/nbs-"+ nav_lang + ".json").then(function(data) {
    console.log(data);


    ////////////////////
    // LAYOUT         //
    ////////////////////

    const margin = 40;

    function get_size(){
        width = window.innerWidth * 0.7;
        height = window.innerHeight -margin;
        if (window.innerWidth < 960){
            mobile = true;
            width = window.innerWidth;
            height = height -40;
        }
        console.log("mobile", mobile);
    };

    const panel_width = 500;

    let mobile = false;
    let width = window.innerWidth * 0.7;
    let height = window.innerHeight -margin;
    get_size();

    const transition_duration = 750;

    const t_slow = d3.transition().duration(transition_duration);
    const t_fast = d3.transition().duration(transition_duration/2);

    let main_size = height;
    if (height > width){
        main_size = width;
    }

    let force_link_length = main_size/11;

    let uc_rad = main_size/3 + main_size/10;
    let uc_size = main_size / 30 ;

    let current_layout = "tree";

    let selected_uc = undefined;
    let selected_node = undefined;

    main_node_size = main_size/18;

    const header = d3.select("body")
          .append("div")
          .attr("id", "nbs-header");

    header.append("a")
        .attr("id", "nbs-header-logo")
        .attr("href", "https://www.nature4cities-platform.eu/");

    const lang_dropdown = header.append("div")
          .attr("class", "nbs-dropdown-lang")
          .on("mouseover", ()=>{
              lang_dropdown.style('height', 'auto');
              console.log("oké");
          })
          .on("mouseleave", () => lang_dropdown.style("height", "25px"));

    const lang_btn = lang_dropdown.append("a")
        .attr("class", "nbs-btn-lang")
          .html(nav_lang+ " <span class='arrow'><sup>\u2304</sup></span>");

    let lst_lang = ['fr', 'en', 'de', 'hu', 'it', 'es', 'tr'];

    lst_lang.forEach(l => {
        if (l != nav_lang){
        lang_dropdown.append("a")
            .attr("class", "nbs-btn-other-lang")
            .html(l)
            .on("click", () => {
                window.location.search = "?hl=" + l;
            });
        }
        // .text("🇫🇷");
    });
    const panel = d3.select("body")
          .append("div")
          .attr("id", "nbs-panel");

    const panel_close = panel.append("div")
          .attr("class", "nbs-panel-close")
          .on("click", () => hide_panel());

    const panel_open = panel.append("div")
          .attr("class", "nbs-panel-open")
          .on("click", () => open_panel());

    const panel_content = panel.append("div")
          .attr("class", "nbs-panel-content");

    const svg = d3.select("body")
          .append("svg")
          .attr("id", "nbs-svg")
          .attr("width", width)
          .attr("height", height)
          .attr("viewBox", [-width / 2, -height / 2, width, height])
          .style("top", margin+"px")
          .style("height", "auto")
          .style("background-color", "black");

    const svg_background = svg.append("rect")
          .attr("fill", "black")
          .attr("x", -width/2)
          .attr("y", -height/2)
          .attr("width", width)
          .attr("height", height)
          .on("click", () => init_style());

    const control = d3.select("body")
          .append("div")
          .attr("id", "nbs-control");

    const info_but = control.append("a")
          .attr("class", "btn btn-info")
          .attr("name", "About")
          .on("click", () => {
              init_style();
              show_infos();
          });

    const search_div = control.append("div")
          .attr("class", "search-div");
    const search_input = search_div.append("input")
          .attr("type", "text");

    let search_focus = false;
    const search_btn = search_div.append("a")
          .attr("class", "btn btn-search")
          .attr("name", "Search")
          .on("click", () => {
              hide_label();
              if (search_focus == true){
                  search_focus = false;
                  search_input.node().value = "";
                search("");
                search_btn
                    .style("background-image", "url('assets/nbs_search.png')");
                search_div.transition(t_slow)
                    // .style("display", "none")
                    .style("width", "40px");
            }else{
                search_focus = true;
                search_btn
                    .style("background-image", "url('assets/nbs_close.png')");
                search_div.transition(t_slow)
                    .style("width", "250px");
            }
        });

    const tree_but = control.append("a")
          .attr("class", "btn btn-tree")
          .attr("name", "Radial layout");

    const force_but = control.append("a")
          .attr("class", "btn btn-force")
          .attr("name", "Force layout");

    const plus_but = control.append("a")
          .attr("class", "btn btn-depth")
          .attr("name", "Expand tree")
          .on("click", ()=>tree_plus());

    plus_but.append("div")
          .text("+");

    const moins_but = control.append("a")
          .attr("class", "btn btn-depth")
          .attr("name", "Collapse tree")
          .on("click", ()=> tree_moins());

    moins_but.append("div")
        .text("-");


    window.onresize =  ()=>{
        console.log("test");
        get_size();
        svg.attr("width", width);
        svg.attr("height", height);
    };

    ////////////////////
    // SEARCHBAR      //
    ////////////////////

    search_input.on("input", d => search(search_input.node().value))
        .on("focus", d => search(search_input.node().value));

    let match = function(d, txt){
        let txt_ = txt.toLowerCase();
        let d_txt = (d.data.description + " " + d.data.name).toLowerCase();
        if (d_txt.includes(txt_)){
            return true;
        }
        return false;
    };

    let search = function(txt){
        console.log("search", txt);
        if (txt != ""){
            root.descendants().forEach( d => {
                if (match(d, txt)){
                    console.log("find", d.data.name);
                    d.hi = true;
                    d.match = true;
                    d.ancestors().forEach(d => d.hi = true);
                }else{
                    d.hi = false;
                    d.match = false;
                }
            });
        }else{
            root.descendants().forEach( d => {
                d.hi = true;
                d.match = false;
            });
        }
        update_style();
    };

    ////////////////////
    // PHOTOS         //
    ////////////////////

    const photo_container = d3.select("body")
          .append("div")
          .attr("id", "nbs-photos-container")
          .style("opacity", 0)
          .style("pointer-events", "none");

    const photo_exit = photo_container.append("div")
          .attr("class", "nbs-photos-exit")
          .on("click", () => {
              photo_container.transition(t_fast)
                  .style("opacity", "0")
                  .style("pointer-events", "none");
          });

    const photo_content = photo_container
          .append("div")
          .attr("id", "nbs-photos-content");

    let create_photo = function(d){
        photo_container.transition(t_fast)
            .style("opacity", 1)
            .style("pointer-events", "auto");

        photo_content.html("");
        photo_content.append("div")
            .append("img")
            .attr("class", "nbs-photo")
            .attr("src", "images/fullsize/" + d.name + ".jpg");

        photo_content.append("div")
            .attr("class", "nbs-photo-legend")
            .append("p")
            .html(d.legend);
    };


    /////////////////////
    // INIT SVG        //
    /////////////////////

    const uc_links_layer = svg.append("g").attr('opacity', 0);

    const link_layer = svg.append("g").attr("fill", "none").attr("id", "link-layer");
    const node_layer = svg.append("g").attr("id", "node-layer");

    const level_layer = svg.append("g")
          .attr("id", "level-layer")
          .attr("opacity", 0)
          .attr("transform", "scale(" + main_node_size/50 + ")")
          .on("mouseover", () => {
              level_layer.transition().attr("opacity", 1);
          })
          .on("mouseleave", ()=>{
              level_layer.transition().attr("opacity", 0);
          });

    level_layer.append("circle")
        .attr("r", 50)
        .attr("fill", color_scale[0]);

    const level_layer_plus = level_layer
          .append("g")
          .attr("id", "btn-plus")
          .on("click", () => tree_plus())
          ;

    level_layer_plus.append("circle")
        .attr("cx", 0)
        .attr("cy", -23)
        .attr("r", 20)
        .attr("fill", d3.color(color_scale[0]).brighter(1));

    level_layer_plus.append("text")
        .attr("y", -15)
        .attr("fill", color_scale[0])
        .text("+");

    const level_layer_moins = level_layer
          .append("g")
          .attr("id", "btn-moins")
          .on("click", () => tree_moins());

    level_layer_moins.append("circle")
        .attr("cx", 0)
        .attr("cy", 23)
        .attr("r", 20)
        .attr("fill", d3.color(color_scale[0]).brighter(1));

    level_layer_moins.append("text")
        .attr("y", 30)
        .attr("fill", color_scale[0])
        .text("-");

    const uc_layer = svg.append("g").attr("id", "uc-layer");

    const label =  d3.select("body").append("div").attr("id", "nbs-label")
          .style("opacity", 0)
          .attr("pointer-events", "none");

    const label_text = label.append("span");


    ////////////////////
    // INIT DATA      //
    ////////////////////

    let t = function(to_translate){
        if (data.traduction[to_translate]){
            if (data.traduction[to_translate][lang]){
                return data.traduction[to_translate][lang];
            }else if(data.traduction[to_translate]["English"]){
                return data.traduction[to_translate]["English"];
            }
        }else{
            console.log("[translation] not found", to_translate);
        }
        return to_translate;
    };

    const tree = d3.cluster().size([2 * Math.PI, main_size/3]);

    let root = tree(d3.hierarchy(data.tree));

    root.descendants()
        .forEach((d, i) => {
            d.id = "n_"+i;
            d.radius = node_size(d);
            d.r_init = d.y;
            d.r = d.y;
            d.a = d.x;
            d.x = d.r * Math.cos(d.a - Math.PI / 2);
            d.y = d.r * Math.sin(d.a - Math.PI / 2);
            d._children = d.children;
            d.show = true;
            d.hi = true;
            d.sel = false;
            d.match = false;
        });

    let number_leaves = root.leaves().length;

    let links = root.links();
    let nodes = root.descendants();

    const tree_uc = d3.cluster();//.size([100, 100]);
    const root_uc = tree_uc(d3.hierarchy({children: data.challenges}));

    root.leaves()
        .forEach(d => {
            d.data.data.uc_t = {};
            root_uc.children.forEach(e => {
                d.data.data.uc_t[e.data.num] = 0;
            });
            for (let [k, uc] of Object.entries(d.data.data.uc)) {
                let num = parseInt(k.split('.')[0]);
                d.data.data.uc_t[num] = Math.max(d.data.data.uc_t[num], uc);
            }
        });

    let nbs_url_node = undefined;
    if (nbs_url){
        var nbs_url_list = root.leaves().filter(d => d.data.key == nbs_url);
        if (nbs_url_list.length == 1){
            nbs_url_node = nbs_url_list[0];
        }
    }


    /////////////////////
    // INIT SIMULATION //
    /////////////////////

    const simulation = d3.forceSimulation(nodes)
        .force("link", d3.forceLink(links)
            .id(d => d.id)
            .distance(d => {
                return force_link_length / (d.source.depth  + 1);
            })
            .strength(4))
        .force("charge", d3.forceManyBody().strength(-150))
        .force("collide", d3.forceCollide(d => {
            if (d.depth == 0) {
                return force_link_length;
            } else {
                return force_link_length/ (d.depth  + 1) + 1;
            }
        }))
        .alpha(0.0)
        .force("x", d3.forceX().strength(d => {
            if (d.depth == 0) {
                return 5;
            } else {
                return 0;
            }
        }))
        .force("y", d3.forceY().strength(d => {
            if (d.depth == 0) {
                return 5;
            } else {
                return 0;
            }
        }));;

    const drag = simulation => {
        function dragstarted(d) {
            if (!d3.event.active) simulation.alphaTarget(0.1).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragended(d) {
            if (!d3.event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

        return d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended);
    };

    simulation.on("tick", () => {
        if (current_layout == "force"){
            link.attr("d", d => {
                    return "M " + d.source.x + " " + d.source.y + " L " + d.target.x + " " + d.target.y;
                });
            node
                .attr("cx", d => d.x)
                .attr("cy", d => d.y);
        }
    });

    // center on click
    if (false) {
        node.on("click", d => {
            simulation.force("x", d3.forceX().strength(n => {
                    if (d == n) {
                        return 5;
                    } else {
                        return 0;
                    }
                }))
                .force("y", d3.forceY().strength(n => {
                    if (d == n) {
                        return 5;
                    } else {
                        return 0;
                    }
                }));

            simulation.alphaTarget(0.1).restart();
        });
    };

    let flip = 0;
    simulation.nodes().forEach(d => {
        if (d.depth == 0) {
            d.fx = 0;
            d.fy = 0;
        }
    });

    sim = simulation;

    let layout_force =function(){

        force_but.classed("active", true);
        tree_but.classed("active", false);

        moins_but.classed("inactive", true);
        plus_but.classed("inactive", true);

        uc_layer.transition(t_slow).attr("opacity", 0);
        uc_links_layer.transition(t_slow).attr("opacity", 0);

       link.transition(t_slow)
           // .delay(transition_duration)
            .attr("d", d => {
                return "M " + d.source.x + " " + d.source.y + " L " + d.target.x + " " + d.target.y;
            });

        console.log("Force layout");
        current_layout = "force";
        setTimeout(() =>{
            simulation.alphaTarget(0.1).restart();
        }, transition_duration);

        setTimeout(() =>{
            simulation.alphaTarget(0);
        }, transition_duration*3);
    };

    force_but.on("click", layout_force);


    //////////////////////////////
    // subchallenges
    //////////////////////////////

    let get_subchallenge = function(d, level){
        let lst_benefit = [];
        root_uc.children.forEach( e => {
            let lst_subchallenge  = [];
            e.children.forEach( c => {
                if (d.data.data.uc[c.data.num] == level){
                    lst_subchallenge.push(c);
                }
            });
            if (lst_subchallenge.length > 0){
                lst_benefit.push({
                    "challenge": e,
                    "subchallenge": lst_subchallenge
                });
            }
        });
        return lst_benefit;
    };

    let create_subchallenge= function(div, lst_benefit){
        let div_benefit = div.append("div");
        lst_benefit.forEach(e => {
            let new_benefit = div_benefit.append("div").attr("class", "challenge");
            new_benefit.append("img")
                .attr("src", e.challenge.data.src)
                .attr("class", "challenge-logo");
            //                    new_benefit.append("h5").html(e.challenge.data.name);
            e.subchallenge.forEach( c => {
                new_benefit.append("h5").html(c.data.name.toLocaleLowerCase());
            });
        });
    };

    const infos_html = d3.create("div")
          .attr("class", "nbs-infos")
          .attr("id", "nbs-infos");
    infos_html.append("h2").text(t("title"));
    infos_html.append("p").text(t("manual_intro"));

    infos_html.append("h3").text(t("Manual"));
    
    infos_html.append("p").text(t("manual_legend"));
    const legend = infos_html.append("div").attr("class", "nbs-legend");
    legend.append("div").text(t("Category")).attr("class", "nbs-legend-category");
    legend.append("div").text(t("Subcategory")).attr("class", "nbs-legend-subcategory");
    legend.append("div").text(t("Type")).attr("class", "nbs-legend-type");
    legend.append("div").text(t("Solution")).attr("class", "nbs-legend-solution");
    infos_html.append("p").text(t("manual_layout"));
    infos_html.append("div").attr("class", "nbs-legend-layout nbs-legend-tree").text(t("manual_radial"));
    infos_html.append("div").attr("class", "nbs-legend-layout nbs-legend-force").text(t("manual_force"));

    if (create_new_button){
        let new_nbs = infos_html.append("a")
            .attr("class", "nbs-new")
            .attr("target", "_blank")
            .attr("href", new_nbs_url)
            .append("h3")
            .text(t("new"));
    }

    infos_html.append("h3").text(t("Credits"));
    infos_html.append("p").html(t("credits_author"));
    infos_html.append("p").html(t("credits_pictos"));
    const license = infos_html.append("p");
    license.append('div').append("a")
        .attr("href", "https://creativecommons.org/licenses/by-sa/4.0/")
        .attr("target", "_blank")
        .append("img")
        .attr("src", require("./assets/cc-by-sa.png").default)
        .attr("height", "20px");

    license.append("span").html(t("credits_contents"));

    let credits_eu = infos_html.append("div")
        .attr("class", "nbs-eu")
        .attr("id", "nbs-eu");

    credits_eu.append("div").append("img")
        .attr("src", require("./assets/eu_logo.png").default)
        .attr("width", "100px");

    credits_eu.append("div")
        .attr("class", "nbs-eu-text")
        .html(t("credits_eu"));

    let infos_active = false;

    let show_infos = function(){
        console.log("show infos");
        if (!infos_active){
            info_but.classed("active", true);
            infos_active = true;

            panel_close.style("display", "block");
            panel_open.style("display", "none");

            if (mobile){
                panel.transition(t_slow)
                    .style("top", margin + "px");
            }else{
                panel_content.transition()
                    .duration(200)
                    .style("opacity", 0);
            }

            setTimeout(function(){
                panel_content.html("");
                panel_content.html(infos_html.node().outerHTML);
                let panel_h = panel.node().getBoundingClientRect().height;
                let panel_content_div = d3.select("#nbs-infos");
                let panel_content_h = panel_content_div.node().getBoundingClientRect().height + 110;
                let credits_eu_div = d3.select("#nbs-eu");
                let credits_eu_h = credits_eu_div.node().getBoundingClientRect().height;
                console.log(panel_h, panel_content_h, credits_eu_h);
                if (panel_content_h + credits_eu_h < panel_h){
                    console.log("y a la place");
                    credits_eu_div.style("position", "absolute");
                    credits_eu_div.style("top", panel_h - credits_eu_h - 50 + "px");
                }else if (panel_content_h - panel_h < credits_eu_h){
                    credits_eu_div.style("position", "absolute");
                    credits_eu_div.style("top", panel_h + "px");
                }

                panel_content.transition()
                    .duration(200)
                    .style("opacity", 1);
            }, 200);
        }
    };
    if (!mobile){
        show_infos();
    }

    let hide_panel = function(){
        infos_active = false;
        info_but.classed("active", false);

        if (selected_node == undefined && selected_uc == undefined){
            panel.transition().duration(750)
                .style("top", "100vh");
        }else{
            let hh = panel_content.select("h2").node().getBoundingClientRect().height;
            panel_close.style("display", "none");
            panel_open.style("display", "block");
            // panel.style("top", "40px");
            // setTimeout(function(){
                panel.transition().duration(750)
                    .style("top", "calc(100vh - " + (hh + 15) +  "px)");
            // }, 100);
        }
    };

    let open_panel = function(){
        console.log(window.innerHeight);
        panel.style("top", window.innerHeight + "px");
        panel.transition()
            .duration(750)
            .style("top", "40px");
        panel_close.style("display", "block");
        panel_open.style("display", "none");
    };

    let show_panel_title = function(){
        panel.style("top", window.innerHeight + "px");
        let hh = panel_content.select("h2").node().getBoundingClientRect().height;
        panel_close.style("display", "none");
        panel_open.style("display", "block");
        panel.transition(t_slow)
            .style("top", "calc(100vh - " + (hh + 15) +  "px)");
    };

    let create_panel_infos = function(d){
            panel_content.html("");
            let abs_path = "> ";
            d.ancestors().reverse().slice(1, 4).forEach(p => {
                if (p.data.name != "empty"){
                    abs_path = abs_path + p.data.name + " > ";
                }
            });
            if (!mobile){
                panel_content.append('p').attr("class", "nbs-breadcrumb").text(abs_path);
            }
            panel_content.append("h2").html(d.data.name);

            if (d.depth == 4){
                let cur_url = new URL(document.URL);
                cur_url.searchParams.set("nbs", d.data.key);

                window.history.replaceState({}, '', cur_url.origin + cur_url.pathname + '?' +  cur_url.searchParams.toString());

                let lst_benefit = get_subchallenge(d, 2);
                let lst_cobenefit = get_subchallenge(d, 1);

                // add pictures
                if (d.data.imgs){
                    d.data.imgs.forEach(i => {
                        panel_content.append("div")
                            .attr("class", "nbs-panel-img")
                            .style("background-image", "url(images/thumbs/" + i.name + ".jpg)")
                            .style("width", "31%")
                            .style("height", "100px")
                            .on("click",() => {
                                console.log(i);
                                create_photo(i);
                            });
                    });
                }

                // add challenges
                panel_content.append("h3").html(t("Challenges"));
                panel_content.append("h4").html(t("Benefits"));
                create_subchallenge(panel_content, lst_benefit);
                panel_content.append("h4").html(t("Co-benefits"));
                create_subchallenge(panel_content, lst_cobenefit);

                // add scale
                panel_content.append("h3").html(t("Scale"));
                let scale_div = panel_content.append("div").attr("class", "challenge");
                scale_div.append("img")
                    .attr("src", "assets/nbs_" + d.data.data.scale + ".png")
                    .attr("class", "challenge-logo");
                scale_div.append("h5").html(t(d.data.data.scale));

                // add description
                panel_content.append("h3").html(t("Short description"));
                panel_content.append("p").html(d.data.description);

                let h_resources = panel_content.append("h3").html(t("resources"));
                // add factsheet link
                let factsheet_url = "pdf/" + nav_lang + "/" + d.data.data.key + "_" + nav_lang + ".pdf";

                let links_container = panel_content.append("ul");
                let download_div = links_container
                    .append("li")
                    .append("a").attr("class", "nbs-download nbs-link")
                    .attr("href", "pdf/" + nav_lang + "/" + d.data.data.key + "_" + nav_lang + ".pdf")
                    .attr("target", "_blank")
                    .html(t("factsheet_link"));

                if (d.data.links){
                    d.data.links.forEach(d => {
                        let link = links_container.append("li")
                            .attr("class", "nbs-link-container")
                            .append("a");
                        link.attr("class", "nbs-link")
                            .attr("target", "_blank");
                        link.attr("href", d["url"]);
                        link.html(d["name"]);
                    });
                }

                url_exists(factsheet_url, () => {
                    download_div.style("display", "none");
                    console.log(d.data.links);
                    console.log(d.data.links == []);
                    if (!d.data.links || d.data.links.length == 0){
                        h_resources.style("display", "none");
                        links_container.style("display", "none");

                    }
                });
            }
    };

    let show_panel = function(d){
        infos_active = false;
        info_but.classed("active", false);
        if (mobile){
            create_panel_infos(d);
            panel.style("top", "100vh")
                .style("opacity", 1);
            let hh = panel_content.select("h2").node().getBoundingClientRect().height;
            panel_close.style("display", "none");
            panel_open.style("display", "block");
            panel.transition(t_slow)
                .style("top", "calc(100vh - " + (hh + 15) +  "px)");

        }else{
            panel_content.transition()
                .duration(200)
                .style("opacity", 0);
            setTimeout(function(){
                create_panel_infos(d);
                panel_content.transition()
                    .duration(200)
                    .style("opacity", 1);
            }, 200);
        }
    };


    //////////////////////////////
    // on node actions
    //////////////////////////////

    let show_label = function(d, marg){
        let pos = cur_elm.node().getBoundingClientRect();

        label_text.html(d.data.name);
        label.style("top", (pos.y - pos.height/2 - marg) + "px")
            .style("left", (pos.x + pos.width/2) + "px")
            .style("margin-left",  -Number(label.style("width").slice(0, -2))/2 + "px");

        label.transition()
            .duration(transition_duration/4)
            .style("opacity", 1);
    };

    let hide_label = function(){
        label.transition()
            .duration(transition_duration/4)
            .style("opacity", 0);
    };

    let show_label_control = function(){
        console.log(this);
        if (this.name != ""){
            if (this.name != "Search" || search_focus == false){
                let pos = this.getBoundingClientRect();
                label_text.html(t(this.name));

                label.style("top", (pos.y - pos.height/2 + 24) + "px")
                    .style("left", (pos.x + pos.width/2) + "px")
                    .style("margin-left",  -Number(label.style("width").slice(0, -2)) - 40 + "px");

                label.transition()
                    .duration(transition_duration/4)
                    .style("opacity", 1);
            }
        }
    };

    d3.selectAll('.btn')
        .on('mouseenter', show_label_control)
        .on('mouseleave', hide_label);
        // .on('mouseleave', hide_label);

    let mouseover_node = function(d){
        if (d.depth != 0 && !mobile){
            cur_elm = d3.select("#" + d.id);
            show_label(d, 20);
        }
    };

    let mouseleave_node = function(d){
        if (d.depth !=0 ){
            hide_label();
        }
    };

    let init_style = function(){
        root.descendants().forEach(e => {
            e.hi = true;
            e.sel = false;
        });
        selected_node = undefined;
        selected_uc = undefined;
        uc_links_g.transition(t_fast).attr("stroke-opacity", 0.0);

        uc.each(d => d.active=false);
        uc_layer.selectAll('.uc-g').classed("active", true);
        if (!mobile){
            show_infos();
        }else{
            panel.style("top", "100vh");
        }
        update_style();
    };

    // nodeclick
    let click_node = function(d){
        console.log(d);

        selected_uc = undefined;
        uc_layer.selectAll('.uc-g').classed("active", false);
        uc.each(d => d.active=false);

        root.descendants().forEach(e => {
            e.hi = false;
            e.sel = false;
        });
        if (d){
            d.ancestors().forEach(e => e.hi = true);
            d.descendants().forEach(e => e.hi = true);
        }else{
            root.descendants().forEach(e => e.hi = true);
        }

        if (selected_node != d){
            selected_node = d;
            d.sel = true;
            show_panel(d);

            uc_links_layer.selectAll("path")
                .attr("d", d3.linkRadial()
                      .angle(d => d.a)
                      .radius(d => d.r));

            uc_links_g
                .transition(t_fast)
                .attr("stroke-opacity", p => {
                    if (p.source == d) {
                        p.target.active=true;
                        return 0.8;
                    } else {
                        return 0.0;
                    }
                });
        }else{
            init_style();
        }
        update_style();
    };

    let click_parent = function(d){
        if (current_layout == "tree"){
            if (d.children){
                d.descendants().forEach(e => {
                    e.show = false;
                });
                d.children = null;
            }else{
                d.children = d._children;
                d.children.forEach(c => {
                    if (c.data.name == "empty"){
                        c.children = c._children;
                        console.log("empty children");
                    }
                });
                d.descendants().forEach(e => e.show = true);
                if (d.depth == cur_depth){
                    cur_depth += 1;
                }
            }
            update_tree();
            init_style();
        }
    };

    let cur_depth = 5;

    let tree_moins = function(){
        if (current_layout == "tree"){
            if (cur_depth == 5){
                cur_depth = 4;
            }
            cur_depth = Math.max(1, cur_depth-1);
            root.descendants().forEach((d, i) => {
                if (d.depth == cur_depth){
                    d.children.forEach(c => c.show = false);
                    d.children = null;
                }
            });
            init_style();
            update_tree();
        }
     };

    let tree_plus = function(){
        if (current_layout == "tree"){
            cur_depth = Math.min(5, cur_depth+1);
            if (cur_depth > 4){
                // cur_depth = 4;
                root.descendants().forEach((d, i) =>{
                    d.children = d._children;
                    if (d.children){
                        d.children.forEach(c => c.show = true);
                    }
                });
                root.leaves().forEach(c => c.show = true);
            }else{
                root.descendants().forEach((d, i) => {
                    if (d.depth == cur_depth-1){
                        d.children = d._children;
                        d.children.forEach(c => c.show = true);
                    }
                });
            }
            init_style();
            update_tree();
        }
    };

    /////////////////////
    // NBS             //
    /////////////////////

    let node, link;

    let update_style = function(){
        node.transition(t_slow)
            .attr("stroke-width", e => {
                if (e.sel == true){
                    return 3;
                }else if (e.match == true){
                    return 1.5;
                }else{
                    return 0;
                }
            }).attr("fill", color);

        link.transition(t_slow).attr("stroke", d => color(d.target));

        uc.each(d => {
            if (d.active){
                uc_layer.selectAll('#uc_'+d.data.num+' .uc-g').classed("active", true);
            }
        });

        uc_layer.selectAll('.uc-g').transition().attr("transform", "scale(0.9)").attr("opacity", 0.5);
        uc_layer.selectAll('.uc-g.active').transition().attr("transform", "scale(1)")
            .attr("opacity", 1);
    };

    let update_tree = function(){
        let max_depth = d3.max(root.descendants().map(d => d.depth));
        if (root.leaves().length == number_leaves){
            max_depth = 5;
        }
        console.log("max_depth = ", max_depth);

        cur_depth = max_depth;

        if (cur_depth == 5){
            plus_but.classed("inactive", true); 
            uc_layer.transition(t_slow).attr("opacity", 1);
            uc_links_layer.transition(t_slow).attr("opacity", 1);
        }else if (cur_depth >= 4){
            uc_layer.transition(t_slow).attr("opacity", 1);
            plus_but.classed("inactive", false); 
            uc_links_layer.transition(t_slow).attr("opacity", 1);
        }else{
            uc_layer.transition(t_slow).attr("opacity", 0);
            uc_links_layer.transition(t_slow).attr("opacity", 0);
            plus_but.classed("inactive", false); 
        }

        moins_but.classed("inactive", cur_depth == 1); 

        root.descendants()
            .forEach((d, i) => {
                d.r = (main_size/3) * d.depth / (Math.min(4, cur_depth));
                d.x0 = d.x;
                d.y0 = d.y;
                d.a0 = d.a;
                d.r0 = d.r;
            });
        root = tree(root);
        root.descendants()
            .forEach((d, i) => {
                d.a = d.x;
                d.x = d.r * Math.cos(d.a - Math.PI / 2);
                d.y = d.r * Math.sin(d.a - Math.PI / 2);
            });

        nodes = root.descendants().reverse();
        links = root.links();

        links.forEach(d => {
            if (d.source.data.name=="empty"){
                console.log("empty link", d);
                d.source = d.source.parent;
            }
        });
        console.log(nodes);
        link = link_layer.selectAll("path")
            .data(links, d => d.target.id)
            .join( enter => enter.append("path")
                   .attr("opacity", d => {
                       if (d.target.data.name=="empty"){
                           return 0;
                       }else{
                           return 1;
                       }
                   })
                   .attr("stroke-width", d => 7 / (d.source.depth + 1))
                   .attr("stroke", d => color(d.target))
                   .attr("d", d => {
                       const o = {x : d.source.a0, y:d.source.r0};
                       return d3.linkRadial()
                         .angle(d => d.x)
                           .radius(d => d.y)({source: o, target:o});
                   })
                   .call(enter => enter
                         .transition(0)
                         .duration(transition_duration/2)
                         .attr("d", d3.linkRadial()
                               .angle(d => d.a)
                               .radius(d => d.r))
                        ),
                   update => update.transition(0)
                   .duration(transition_duration/2)
                   .attr("d", d3.linkRadial()
                         .angle(d => d.a)
                         .radius(d => d.r)),
                   exit => exit.transition(transition_duration)
                   .attr("d", d => {
                       const o = {x : d.source.a0, y:d.source.r0};
                       return d3.linkRadial()
                           .angle(d => d.x)
                           .radius(d => d.y)({source: o, target:o});
                   })
                   .attr("opacity", 0).remove());

        node = node_layer.selectAll("circle").data(nodes, d => d.id)
            .join(
                enter => enter.append("circle")
                    .attr("id", d => d.id)
                    .attr("fill", color)
                    .attr("stroke", "white")
                    .attr("stroke-width", "0")
                    .attr("r", node_size)
                    .attr("cx", d => {if (d.parent){return d.parent.x0;}else{return 0;}})
                    .attr("cy", d => {if (d.parent){return d.parent.y0;}else{return 0;}})
                    .attr("opacity", 1)
                    .on("mouseenter", mouseover_node)
                    .on("mouseleave", mouseleave_node)
                    .on("click", d => {
                        if (d.depth < 4 && d.depth > 0){
                            click_parent(d);
                        }else{
                            click_node(d);
                        }
                    }).call(enter => enter.transition(0)
                            .duration(transition_duration/2)
                            .attr("cx", d => d.x)
                            .attr("cy", d => d.y)
                           ),

                update => update.transition(0)
                    .duration(transition_duration/2)
                    .attr("cx", d => d.x)
                    .attr("cy", d => d.y),

                exit => exit.transition(transition_duration)
                    .attr("opacity", 0)
                    .attr("cx", d => {if (d.parent){return d.parent.x;}else{return 0;}})
                    .attr("cy", d => {if (d.parent){return d.parent.y;}else{return 0;}})
                    .remove()
            );

    };

    let layout_tree = function(){
        console.log("layout tree");
        current_layout = "tree";

        force_but.classed("active", false);
        tree_but.classed("active", true);
        simulation.alphaTarget(0);
        update_tree();
    };

    layout_tree();
    // init_style();

    // show_infos();

    tree_but.on("click", layout_tree);

    /////////////////////
    // INIT UC         //
    /////////////////////

    const uc_data = data.challenges;
    let n_uc = uc_data.length;

    const uc_map = {};
    uc_data.forEach(d => {
        d.children.forEach(c => {
            uc_map[c.num] = d;
        });
    });
    const uc_map2 = {};
    root_uc.leaves().forEach(d => {
        uc_map2[d.data.num] = d;
    });

    root_uc.descendants().forEach(d => {
        if (d.depth == 1){
            let i = d.data.num -1;
            d.r = uc_rad;
            d.a = Math.PI * 2 * i / n_uc + Math.PI / 2;
            d.x_rad = uc_rad * Math.cos(Math.PI * 2 * i / n_uc);
            d.y_rad = uc_rad * Math.sin(Math.PI * 2 * i / n_uc);
            d.children.forEach(c => {
                c.r = d.r - 20;
                c.a = d.a;
            });
            let img_src = require(`./assets/uc_${d.data.num}.png`);
            d.data.src = img_src.default;
        }
    });

    const uc_links = [];

    root.leaves().forEach(d => {
        for (let [k, uc] of Object.entries(d.data.data.uc)) {
            if (uc != 0) {
                uc_links.push({
                    source: d,
                    target: uc_map2[k].parent,
                    value: uc
                });
            }
        }
    });

    const uc_links_g = uc_links_layer.append("g")
          .attr("stroke", "lightgray")
        .attr("fill", "none")
        .selectAll("path")
        .data(uc_links)
        .enter()
        .append("path")
          .attr("stroke-width", d => d.value)
          .attr("stroke-opacity", 0.0)
        .attr("d", d3.linkRadial()
            .angle(d => d.a)
            .radius(d => d.r));

    const uc = uc_layer.selectAll("g")
          .data(root_uc.children, d => d.data.num)
          .join("g")
          .attr("id", d => "uc_"+d.data.num)
          .attr("transform", d => {
              let x = 0;
              let y = 0;
              return "translate(" + x + "," + y + ")";
          });

    uc.transition(t_slow)
        .filter(d => d.depth == 1)
        .attr("transform", d => {
            let x = d.x_rad;
            let y = d.y_rad;
            return "translate(" + x + "," + y + ")";
        });

    let uc_g = uc.append("g")
        .attr("class", "uc-g")
        .attr("transform", "scale(1)")
        .attr("opacity", 1);

    uc_g.append('circle')
        .attr('fill', '#111')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', uc_size);
    uc_g.append('image')
        .attr("xlink:href", d => d.data.src)
        .attr("width", uc_size * 2)
        .attr("height", uc_size * 2)
        .attr("x", -uc_size)
        .attr("y", -uc_size);
    uc_g.append('circle')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', uc_size)
        .attr('fill', '#000')
        .attr('opacity', 0.2);


    uc.on("mouseleave", mouseleave_node);
    uc.on("mouseover", d => {
        cur_elm = uc_layer.select("#uc_"+d.data.num);
        show_label(d, 10);
    });

    // ucclick
    uc.on("click", d => {
        // create panel content
        infos_active = false;
        info_but.classed("active", false);
        panel_content.html("");
        let title = panel_content.append("h2");
        title.append("img")
            .attr("src", d.data.src)
            .attr("class", "challenge-logo");
        title.append("div").html(d.data.name);
        panel_content.append("p").html(d.data.description);
        d.children.forEach(c =>{
            panel_content.append("h3").html(c.data.name);
            panel_content.append("p").html(c.data.description);
        });

        if (mobile){
            show_panel_title();
        };

        // change style of elements
        if (selected_uc != d){
            selected_uc = d;
            uc_links_layer.selectAll("path")
                .attr("d", d3.linkRadial()
                      .angle(d => d.a)
                      .radius(d => d.r));

            uc_links_g
                .transition(t_slow)
                .attr("stroke-opacity", p => {
                    if (p.target == d) {
                        if (p.source.show == true){
                            return 0.5;
                        }
                    }
                    return 0.0;
                });

            root.descendants().forEach(e => e.hi = false);
            node.filter(n => n.data.data)
                .each(p => {
                    if (p.data.data.uc_t[d.data.num] != 0) {
                        p.ancestors().forEach(e => e.hi = true);
                        p.hi = true;
                    }else{
                        p.hi = false;
                    }
                });

            uc.each(d => d.active=false);
            uc_layer.selectAll('.uc-g').classed("active", false);
            uc_layer.selectAll('#uc_'+d.data.num+' .uc-g').classed("active", true);
            // uc_layer.selectAll('.uc-g').transition().attr("transform", "scale(1)");
            // uc_layer.selectAll('#uc_'+d.data.num+' .uc-g').transition().attr("transform", "scale(1.2)");

            update_style();
        }else{
            selected_uc = undefined;
            init_style();
        }
    });
    svg.call(d3.zoom().on("zoom", zoomed)).on("wheel.zoom", wheeled);

     let transform = d3.zoomTransform(svg);
    transform.x = 0;
     transform.y = 0;
     svg.attr("transform", transform);

    if (nbs_url_node){
        click_node(nbs_url_node);
    }

     function zoomed() {
        let current_transform = d3.zoomTransform(svg);
        if (d3.event.sourceEvent.type === "touchmove" && d3.event.sourceEvent.touches.length > 1){
            current_transform.k = d3.event.transform.k;
        }
        current_transform.x += d3.event.transform.x;
        current_transform.y += d3.event.transform.y;
        svg.attr("transform", current_transform);
    }

    function wheeled() {
        let current_transform = d3.zoomTransform(svg);
        current_transform.k = current_transform.k - d3.event.deltaY * 0.01;
        console.log(current_transform);
        svg.attr("transform", current_transform);
}});
