import fs from 'fs';
import XLSX from 'xlsx';
import process from "./test/nbs_data_process.mjs";


let workbook = XLSX.readFile("test/nbs.xlsx");
let [jsons, stats] = process(workbook);

let string_json = JSON.stringify(jsons["nbs-fr"], null, 2);

fs.writeFile("test/out.json", jsons["nbs-fr"], (e, d) => console.log(e));
console.log(Object.keys(jsons));
