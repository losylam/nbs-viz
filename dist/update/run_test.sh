#!/bin/sh

echo "import XLSX from 'xlsx';\n\n" > test/nbs_data_process.mjs
cat nbs_data_process.js >> test/nbs_data_process.mjs

node --experimental-modules nbs_data_test.js

diff test/out.json test/ref.json
