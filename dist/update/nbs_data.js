// var workbook = XLSX.readFile("http://localhost:8000/nbs.xlsx");
// let XLSX = require("xlsx");
// let fs = require("fs");
import nbs_data_process from "./nbs_data_process.js";

$( document ).ready(function(){

    var process_done = false;

    ////////////////////////
    // UTILS              //
    ////////////////////////

    function log(txt){
        $(".log").append(txt + "\n");
    }

    ////////////////////////
    // PROCESS            //
    ////////////////////////

    var out_jsons = {};
    function process(workbook){

        let stats = {};
        [out_jsons, stats] = nbs_data_process(workbook);

        let lang_stat = stats.lang;
        let table_sizes = stats.tables;

        log_table_sizes(table_sizes);
        log_lang_stat(lang_stat);
        process_done = true;
        $(".disabled").removeClass("disabled");
    }

    function log_table_sizes(table_sizes){
        console.log(table_sizes);
        log("// NUMBER OF LINES IN TABLE //");
        for (const [key, it] of Object.entries(table_sizes)){
            log("- " + key + "  \t" + it);
        }
        log("");

    }

    function log_lang_stat(lang_stat){
        console.log(lang_stat);
        log("// TRANSLATION SCORE //");
        for (const [key, it] of Object.entries(lang_stat)){
            log("- " + it.name + "  \t" + Math.round(it["found"]*100/it["process"]) + "%");
        }
        log("");
    }

    function download(){
        var zip = new JSZip();
        for (const [filename, json_string] of Object.entries(out_jsons)){
            zip.file(filename + ".json", json_string);
        }
        zip.generateAsync({type:"blob"})
            .then(function (blob) {

                var a = document.createElement("a");
                a.href = URL.createObjectURL(blob);
                a.download = "nbs-data.zip";
                a.click();
            });
    }

    $("#fileinput").on("change", function(){
        var file = $('#fileinput').prop('files')[0];
        console.log(file);
        log("// LOAD FILE //");
        log("Process file: " + file.name);
        log("");
        var reader = new FileReader();
        reader.onload = function(e){
            var data = e.target.result;
            var workbook = XLSX.read(data, {type:"array"});
            process(workbook);
            console.log(workbook);
        };
        reader.readAsArrayBuffer(file);
    });

    function dropHandler(ev){
        console.log(ev);
        ev.preventDefault();
        var file = ev.dataTransfer.items[0].getAsFile();
        var reader = new FileReader();
        reader.onload = function(e){
            var data = e.target.result;
            var workbook = XLSX.read(data, {type:"array"});
            process(workbook);
            console.log(workbook);
        };
        reader.readAsArrayBuffer(file);
    }

    $("#drop_zone").on("drop", function(event){
        dropHandler(event);
    });

    $("#download").click(function(){
        console.log("download");
        if (process_done) download();
    });

    $("#update-test").click(function(){
        console.log("update-test", out_jsons);
        log("// UPDATE TO TEST //");
        if (process_done){
            var out_test = {"test": true};

            for (const [key, item] of Object.entries(out_jsons)){
                out_test[key] = item;
            }

            console.log(out_test);

            $.post(
                'update.php',
                out_test,
                function (data, textStatus, jqXHR){
                    console.log(data);
                    log(data);
                }
            );
        }
        log("");
    });

    $("#update").click(function(){
        console.log("update", out_jsons);
        log("// UPDATE //");
        if (process_done){
            $.post(
                'update.php',
                out_jsons,
                function (data, textStatus, jqXHR){
                    console.log(data);
                    log(data);
                }
            );
        }
        log("");
    });
});
