// import XLSX from 'xlsx';

var start_at = 4;
var header = ["key",
              "category",
              "subcategory",
              "class",
              "name"];

var lst_scales = ["Entity",
                  "District",
                  "City"];

var lst_challenges = [];

var start_challenges = 8;

var nbs_list = [];
var nbs_dic = {};

var langs = [];
var lang_stat = {};

const lang_ext = {
    "English": "en",
    "Français": "fr",
    "Magyar": "hu",
    "Deutsch": "de",
    "español": "es",
    "Italian": "it",
    "Turkish": "tr"
};

var table_sizes;

////////////////////////
// UTILS              //
////////////////////////

function get_cell(sheet, r, c){
    let cell = sheet[XLSX.utils.encode_cell({r: r, c: c})];
    if (cell){
        return cell.v;
    }else{
        return undefined;
    }
}

function has_children(parent_el, child_name){
    return parent_el.children.map(x => x.name).includes(child_name);
}

function get_nbs_data(sheet, num_nbs, lst_challenges){
    let data = {'uc':{}};
    // console.log(get_cell(sheet, 10, 3).v);
    for (var pos_h = 0; pos_h < header.length; pos_h++){
        var val = get_cell(sheet, num_nbs, pos_h );
        if (val == undefined){
            data[header[pos_h]] = "empty";
        }else{
            data[header[pos_h]] = val;
        }
    }

    for (var pos_e = 0; pos_e < 3; pos_e++){
        if (get_cell(sheet, num_nbs, header.length+pos_e) == "1") {
            data["scale"] = lst_scales[pos_e];
        }
    }
    for (var pos_c = 0; pos_c < lst_challenges.length; pos_c++){
        var challenge = lst_challenges[pos_c];
        // console.log(num_nbs, challenge.c);
        let gcell = get_cell(sheet, num_nbs, challenge.c);
        if (gcell){
            data.uc[challenge.num] = gcell;
        }else{
            data.uc[challenge.num] = 0;
        }
    }

    return data;
}

function init_lang_stat(){
    var lang_stat = {};
    for (let [key, value] of Object.entries(lang_ext)){
        lang_stat[key] = {"name": key,
                          "process": 0,
                          "found": 0,
                          "not_found": []};
    }
    console.log("lang_stat", lang_stat);
    return lang_stat;
}

function parse_header(sheet){
    var challenges = {};
    var name_challenge = "";
    var end_challenges = start_challenges;
    while (get_cell(sheet, 3, end_challenges) !== undefined){
        end_challenges += 1;
    }
    for (var pos = start_challenges; pos < end_challenges; pos +=1){
        var name_subchallenge = get_cell(sheet, 3, pos);
        var num_subchallenge = get_cell(sheet, 2, pos).toString();
        // console.log(num_subchallenge);
        if (num_subchallenge.split(".")[1] == 1){
            name_challenge = get_cell(sheet, 1, pos);
            challenges[name_challenge] = {type: "challenge",
                                          name: name_challenge,
                                          subchallenges: []};
        }
        // console.log(num_subchallenge);

        var challenge = {type: "subchallenge",
                         name: name_subchallenge,
                         num: num_subchallenge,
                         c: pos,
                         parent: name_challenge};

        challenges[name_challenge].subchallenges.push(challenge);

        lst_challenges.push(challenge);
    }
    return lst_challenges;
}


function parse_as_tree(sheet){
    lst_challenges = parse_header(sheet);

    var nbs_tree = {name: "nbs",
                    children: []};
    var cur_cat, cur_subcat, cur_class;
    var cur_nbs_num = start_at;
    var cur_nbs = get_cell(sheet, cur_nbs_num, 4);
    var cur_id = 0;
    while (cur_nbs !== undefined){
        var nbs_key = get_cell(sheet, cur_nbs_num, 0);
        var nbs_category = get_cell(sheet, cur_nbs_num, 1);

        var has_subcategory = get_cell(sheet, cur_nbs_num, 2);
        var nbs_subcategory = "empty";
        if (has_subcategory !== undefined){
            nbs_subcategory = get_cell(sheet, cur_nbs_num, 2);
        }

        var has_class = get_cell(sheet, cur_nbs_num, 3);
        var nbs_class = "empty";
        if (has_class !== undefined){
            nbs_class = get_cell(sheet, cur_nbs_num, 3);
        }

        var nbs_name = get_cell(sheet, cur_nbs_num, 4);

        if (!has_children(nbs_tree, nbs_category)){
            cur_cat = {
                type: "category",
                name: nbs_category,
                children: []};
            nbs_tree.children.push(cur_cat);
        }

        if (!has_children(cur_cat, nbs_subcategory)){
            cur_subcat = {
                type: "subcategory",
                name: nbs_subcategory,
                children: []};
            cur_cat.children.push(cur_subcat);
        }

        if (!has_children(cur_subcat, nbs_class)){
            cur_class = {
                type: "class",
                name: nbs_class,
                children: []};
            cur_subcat.children.push(cur_class);
        }

        var new_nbs = {
            type: "nbs",
            key: nbs_key,
            name: nbs_name,
            orig_name: nbs_name,
            id: cur_nbs_num-start_at,
            data: get_nbs_data(sheet, cur_nbs_num, lst_challenges)};
        nbs_list.push(new_nbs);
        nbs_dic[nbs_key] = new_nbs;
        cur_class.children.push(new_nbs);

        cur_nbs_num += 1;
        cur_nbs = get_cell(sheet, cur_nbs_num, 4);
    }

    table_sizes["Matrix"] = cur_nbs_num;

    return nbs_tree;
}



////////////////////////
// TRADUCTION TABLE   //
////////////////////////


function get_traduction_list(sheet, cell_y, cell_x, lang){
    let elem_trad = {};
    for (let i = 0; i < lang.length; i++){
        let cur_elem_trad = get_cell(sheet, cell_y, cell_x+i);
        lang_stat[lang[i]]["process"] += 1;
        if (cur_elem_trad){
            elem_trad[lang[i]] = cur_elem_trad;
            lang_stat[lang[i]]["found"] += 1;
        }else{
            lang_stat[lang[i]]["not_found"].push(elem_trad["English"]);
            elem_trad[lang[i]] = "";
        }
    }
    return elem_trad;
}

function get_traduction(sheet){
    let trad = {};
    langs = [];
    let pos_lang = 1;
    let cur_lang = get_cell(sheet, 0, pos_lang);
    while (cur_lang){
        langs.push(cur_lang);
        pos_lang += 1;
        cur_lang = get_cell(sheet, 0, pos_lang);
    }

    let pos_elem = 1;
    let cur_elem = get_cell(sheet, pos_elem, 1);
    while (cur_elem){
        let elem_en = cur_elem;
        let elem_trad = get_traduction_list(sheet, pos_elem, 1, langs);
        let elem_key = get_cell(sheet, pos_elem, 0);
        if (pos_elem > 140 && elem_key){
            trad[elem_key] = elem_trad;
        }else{
            trad[elem_en] = elem_trad;
        }
        pos_elem += 1;
        cur_elem = get_cell(sheet, pos_elem, 1);
    }

    table_sizes["traduction"] = pos_elem;

    return trad;
}

let not_found = [];
function translate_elem(obj, lang, cur_lang){
    if (Array.isArray(obj)){
        obj.forEach(d => translate_elem(d, lang, cur_lang));
    }
    if ("name" in obj && "type" in obj && obj.type != "nbs"){
        if (obj.name in lang){
            obj.name = lang[obj.name][cur_lang];
        }else{
            not_found.push(obj.name);
            console.log("not found in", cur_lang, " -> ", obj.name);
        }
    }

    if ("children" in obj){
        for (let i=0; i < obj.children.length; i ++){
            translate_elem(obj.children[i], lang, cur_lang);
        }
    }
}

function translate(obj, lang, cur_lang){
    let json_loc = JSON.parse(JSON.stringify(obj));
    translate_elem(json_loc, lang, cur_lang);
    return json_loc;
}

////////////////////////
// INFO NBS           //
////////////////////////

function get_infos(workbook){
    console.log("get_infos");
    let nbs_infos = get_info_nbs(workbook.Sheets[workbook.SheetNames[2]]);
    let imgs_infos = get_info_images(workbook.Sheets[workbook.SheetNames[4]]);

    // console.log(nbs_infos);

    for (let [key, value] of Object.entries(nbs_infos)){
        if (value.imgs){
            for (let i=0; i < value.imgs.length; i ++){
                let img_data = imgs_infos[value.imgs[i].trim()];
                if (!img_data){
                    img_data = {"name" : value.imgs[i].trim(),
                                "legend": undefined};
                }
                value.imgs[i] = img_data; 
            }
        }else{
            value.imgs = [];
            console.log("no images found for ", value.key);
        }
    }
    console.log(nbs_infos["PG_largepark"]);
    return nbs_infos;
}

function get_info_nbs(sheet){
    let pos_meta = 1;
    let cur_meta = get_cell(sheet, pos_meta, 1);
    let infos_nbs = {};
    let cur_nbs = undefined;
    while (cur_meta && cur_meta.trim() != ""){
        if (cur_meta == "Nom NBS"){
            // let name = get_cell(sheet, pos_meta, 2);
            let key = get_cell(sheet, pos_meta, 0);
            let name = get_traduction_list(sheet, pos_meta, 2, langs);
            cur_nbs = {"name": name,
                       "links": []};
            infos_nbs[key] = cur_nbs;
        }else if (cur_meta == "description"){
            cur_nbs["description"] =  get_traduction_list(sheet, pos_meta, 2, langs);
        }else if (cur_meta == "lien fiche"){
            cur_nbs["lien_fiche"] = get_cell(sheet, pos_meta, 2);
        }else if (cur_meta.indexOf("photos") >= 0){
            let cc = get_cell(sheet, pos_meta, 2);
            if (cc){
                cur_nbs["imgs"] = cc.split(',');
            }
        }else if(cur_meta.indexOf("lien") >= 0){
            let pos_end = find_next_meta(sheet, pos_meta);
            let links = get_links(sheet, pos_meta, pos_end);
            pos_meta = pos_end - 1;
            cur_nbs["links"] = links;
        }else{
            console.log("unknown meta", cur_meta);
        }
        pos_meta += 1;
        cur_meta = get_cell(sheet, pos_meta, 1);
    }

    table_sizes["infos NBS"] = pos_meta;

    //console.log("nbs_infos", infos_nbs);

    return infos_nbs;
}

function get_links(sheet, pos_start, pos_end){
    console.log("get_links");
    console.log("pos_meta", pos_start, pos_end);
    let dic_links = {};
    let lang_lst = Object.keys(lang_ext);
    for (var lang_pos = 0; lang_pos < lang_lst.length; lang_pos++){
        let lang_name = lang_lst[lang_pos];
        dic_links[lang_name] = [];

        for (var line_pos = pos_start; line_pos < pos_end; line_pos++){
            let val = get_cell(sheet, line_pos, lang_pos+2);
            if (val){
                let dic_link = parse_link(val);
                dic_links[lang_name].push(dic_link);
            }
        }
    }
    return dic_links;
}

function parse_link(link){
    let link_list = link.trim().split(" ");
    let dic_link = {"name": undefined,
                    "url": undefined};
    if (link.length > 0){
        let url = link_list[link_list.length-1];
        let start_url = link.indexOf(url);
        dic_link["name"]= link.slice(0, start_url-1);
        dic_link["url"] = url;
    }else{
        dic_link["url"] = link;
    }
    return dic_link;
}

function find_next_meta(sheet, pos_start){
    let cur_pos = pos_start;
    let cur_val = undefined;
    let tot = 0;
    while (cur_val == undefined && tot<20){
        cur_pos += 1;
        tot += 1;
        cur_val = get_cell(sheet, cur_pos, 1);

        if (cur_val!= undefined && cur_val.indexOf("lien")>=0){
            cur_val = undefined;
        }
        console.log('cur_val',  cur_pos, cur_val);
    }

    return cur_pos;
}

function get_info_images(sheet){
    let dic_imgs = {};
    let pos_img = 1;
    let cur_img = get_cell(sheet, pos_img, 0);
    while (cur_img){
        let new_img = {"name": cur_img,
                       "legend": get_traduction_list(sheet, pos_img, 1, langs)};
        dic_imgs[cur_img] = new_img;
        pos_img +=1;
        cur_img = get_cell(sheet, pos_img, 0);
    }

    table_sizes["pictures"] = pos_img;

    return dic_imgs;
}


function add_infos(obj, cur_lang, nbs_infos){
    if ("children" in obj){
        for (let i=0; i < obj.children.length; i ++){
            add_infos(obj.children[i], cur_lang, nbs_infos);
        }
    }else{
        if (nbs_infos[obj.key]){
            obj.description = nbs_infos[obj.key].description[cur_lang];
            obj.name = nbs_infos[obj.key].name[cur_lang];
            obj.links = nbs_infos[obj.key].links[cur_lang];
            let imgs = [];
            nbs_infos[obj.key].imgs.forEach(d => {
                if (d){
                    let new_img = {"name": d.name};
                    if (d.legend) new_img["legend"] = d.legend[cur_lang];
                    imgs.push(new_img);
                }
            });
            obj.imgs = imgs;
        }
    }
}

////////////////////////
// PROCESS CHALLENGES //
////////////////////////


function get_challenges_description(sheet){
    let challenges_description = {};
    let pos = 1;
    let cur = get_cell(sheet, pos, 0);
    while (cur){
        let challenge_key = cur;
        let description = get_traduction_list(sheet, pos, 2, langs);
        challenges_description[challenge_key] = description;
        pos += 1;
        cur = get_cell(sheet, pos, 0);
    }

    table_sizes["Challenges"] = pos;

    return challenges_description;
}

function add_challenges_description(challenges, challenges_description, cur_lang){
    challenges.forEach(d => {
        if (challenges_description[d.num.toString()][cur_lang] != ''){
            d.description = challenges_description[d.num.toString()][cur_lang];
        }else{
            d.description = challenges_description[d.num.toString()]['English'];
        }
        d.children.forEach( c => {
            if (challenges_description[c.num.toString()][cur_lang] != ''){
                c.description = challenges_description[c.num.toString()][cur_lang];
            }else{
                c.description = challenges_description[c.num.toString()]['English'];
            }
        });
    });
    return challenges;
}

function process_challenges(){
    let challenges = [];
    let cur_parent = "";
    let cur_challenge;
    lst_challenges.forEach(d => {
        let p = d.parent;
        if (p !== cur_parent){
            let num = parseInt(d.num.split(".")[0]);
            cur_parent = p;
            cur_challenge = {
                type: "challenge",
                name_orig: p,
                name: p,
                num: num,
                src: "images/uc_" + num + ".png",
                children: [d]
            };
            challenges.push(cur_challenge);
        }else{
            cur_challenge.children.push(d);
        }
    });
    return challenges;
}

function process_lang(json_data, nbs_infos, challenges, challenges_description, trad, lang){
    let challenges_trad = translate(challenges, trad, lang);
    let json_trad = translate(json_data, trad, lang);

    add_challenges_description(challenges_trad, challenges_description, lang);
    add_infos(json_trad, lang, nbs_infos);

    let out_json =  {"tree": json_trad, "challenges": challenges_trad, "traduction": trad};
    let string_json = JSON.stringify(out_json, null, 2);
    console.log("save json for ", lang);
    // fs.writeFile("nbs-" + lang_ext[lang] + ".json", string_json, (e, d) => console.log(e));
    return string_json;
};


export default function process(workbook){
    table_sizes = {"Matrix":0,
                   "traduction": 0,
                   "infos NBS": 0,
                   "Challenges": 0,
                   "pictures": 0};

    lst_challenges = [];
    lang_stat = init_lang_stat();
    var out_jsons = {};

    let trad = get_traduction(workbook.Sheets[workbook.SheetNames[1]]);
    let nbs_infos = get_infos(workbook);

    //console.log("trad", trad);

    var sh = workbook.Sheets[workbook.SheetNames[0]];
    var jsonData = parse_as_tree(sh);
    //console.log("jsonData: ", jsonData);
    var challenges = process_challenges();

    let challenges_description = get_challenges_description(workbook.Sheets[workbook.SheetNames[3]]);

    for (const [lang_name, lang_abbrev] of Object.entries(lang_ext)){
        let json_string = process_lang(jsonData, nbs_infos, challenges, challenges_description, trad, lang_name);
        out_jsons["nbs-"+lang_abbrev] = json_string;
    }
    return [ out_jsons, {lang: lang_stat, tables: table_sizes } ];
}

export function test(){
    console.log("hello there");
}

// export { process, test }

