<?php
if (!empty($_POST))
{
    $jsonNameArray = array("nbs-fr", "nbs-en", "nbs-de", "nbs-hu", "nbs-it", "nbs-tr", "nbs-es");
    $pathOut = "../data/";
    $ok = 0;

    if (array_key_exists("test", $_POST))
    {
        echo "-> update to test: start\n";
        $pathOut = "../test/data/";
    }else{
        echo "-> update: start\n";
        // $pathOut = "../test/data/";
    }

    foreach ($jsonNameArray as $jsonName)
    {
        echo "get:\t" . $jsonName . "\n";
        if (array_key_exists($jsonName, $_POST))
       {
           $filename = $pathOut . $jsonName . ".json";
           echo "write:\t" . $filename . "\n";
           $file_put = file_put_contents($filename,  $_POST[$jsonName]);
           $ok +=  1;
        }
    }

    echo "\n";


    $success = "failed";
    if ($ok == 7){
        $success = "done";
    }

    if (array_key_exists("test", $_POST))
    {
        echo "-> update to test:" . $success . "\n";
    } else {
        echo "-> update: " . $success . "\n";
    }
}
?>
